import axios from "axios";
import React, { useEffect, useState } from "react";
function UpdateForm({ articleUpdate, setArticleUpdate, fetchArticles }) {
  const [input, setInput] = useState({
    name: "",
    content: "",
    image_url: "",
    highlight: false,
  });

  const handleChange = (event) => {
    if (event.target.name === "name") {
      setInput({ ...input, name: event.target.value });
    } else if (event.target.name === "content") {
      setInput({ ...input, content: event.target.value });
    } else if (event.target.name === "image_url") {
      setInput({ ...input, image_url: event.target.value });
    } else if (event.target.name === "highlight") {
      setInput({ ...input, highlight: event.target.checked }); // checkbox
    }
  };

  const handleUpdate = async () => {
    try {
      const response = await axios.put(
        `https://api-project.amandemy.co.id/api/articles/${articleUpdate.id}`,
        {
          name: input.name,
          image_url: input.image_url,
          content: input.content,
          highlight: input.highlight,
        }
      );
      setArticleUpdate(false);
      fetchArticles();
      alert("Berhasil Mengupdate Article");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  useEffect(() => {
    setInput({
      name: articleUpdate.name,
      content: articleUpdate.content,
      image_url: articleUpdate.image_url,
      highlight: articleUpdate.highlight,
    });
  }, []);

  return (
    <div className="my-6 border-2 border-black rounded-xl p-8 max-w-2xl mx-auto ">
      <h1 className="text-2xl font-bold text-center mb-6">
        Form Update Articles
      </h1>

      <div className="grid grid-cols-6 gap-y-6">
        <label htmlFor="" className="col-span-2">
          Name
        </label>
        <p>:</p>
        <input
          onChange={handleChange}
          type="text"
          placeholder="Masukkan Nama"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
          name="name"
          value={input.name}
        />
        <label htmlFor="" className="col-span-2">
          Konten
        </label>
        <p>:</p>
        <input
          onChange={handleChange}
          name="content"
          value={input.content}
          type="text"
          placeholder="Masukkan Konten"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
        />
        <label htmlFor="" className="col-span-2">
          Image URL
        </label>
        <p>:</p>
        <input
          onChange={handleChange}
          type="text"
          value={input.image_url}
          name="image_url"
          placeholder="Masukkan Image URL"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
        />
        <input
          type="checkbox"
          checked={input.highlight}
          onChange={handleChange}
          name="highlight"
        />
        <label htmlFor="" className="col-span-3">
          Is Highlight?
        </label>
      </div>
      <div className="flex items-center justify-center">
        <button
          onClick={handleUpdate}
          className="bg-green-300 border border-green-600 px-6 py-2 mt-5"
        >
          Update
        </button>
      </div>
    </div>
  );
}

export default UpdateForm;
