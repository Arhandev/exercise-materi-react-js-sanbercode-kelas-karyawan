import axios from "axios";
import { useEffect, useState } from "react";
import "./App.css";
import Article from "./components/Article";
import Form from "./components/Form";

function App() {
  const [articles, setArticles] = useState([]);

  const fetchArticles = async () => {
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/articles"
      );
      setArticles(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };


  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <div className="my-8">
      <Form fetchArticles={fetchArticles} />

      <h1 className="text-3xl font-bold text-center mt-6 mb-4">Articles</h1>
      <div className="flex flex-col justify-center gap-6 mt-4 max-w-2xl mx-auto">
        {articles.map((item, index) => {
          return <Article article={item} />;
        })}
      </div>
    </div>
  );
}

export default App;
